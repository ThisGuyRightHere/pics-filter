angular.module("picsFilter")
.directive("filterMenu", function(filterMenuFactory, $rootScope){
    return {
        restrict: 'E',
        scope:{},
        controllerAs: "filterMenuCtrl",
        templateUrl: "client/filter-menu/filter-menu.ng.html",
        controller: function() {
            var that = this;
            this.filterMenuOptions = filterMenuFactory.filters;
            this.changeFilter = function(newFilter) {
                console.log("This would change the filter to ", newFilter);
                $rootScope.$broadcast("filterChangedTo", newFilter);
            };
        }
    }
});